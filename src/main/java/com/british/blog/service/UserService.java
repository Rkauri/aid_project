package com.british.blog.service;

import java.util.Set;

import com.british.blog.model.User;
import com.british.blog.security.PasswordResetToken;
import com.british.blog.security.UserRole;

public interface UserService {
	PasswordResetToken getPasswordResetToken(final String token);
	
	void createPasswordResetTokenForUser(final User user, final String token);
	
	User findByUsername(String username);
	
	User findByEmail (String email);
	
	User findById(Long id);
	
	User createUser(User user, Set<UserRole> userRoles) throws Exception;
	
	User save(User user);

}
