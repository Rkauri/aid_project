package com.british.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.british.blog.dao.ArticleDao;
import com.british.blog.model.Article;
import com.british.blog.model.Catagory;
@Service
public class ArticleServiceImpl implements ArticleService {
@Autowired
ArticleDao articleDao;
	
	@Override
	public Article addArticle(Article article) {
		return articleDao.save(article);
		
	}

	@Override
	public Article updateArticle(Article article) {
	return articleDao.save(article);		
	}

	@Override
	public List<Article> getArticles() {
		
		return articleDao.findAll();
	}

	@Override
	public List<Article> getArticleByCatagory(Catagory catagory) {
		
		return articleDao.findByCatagory(catagory);
	}

	@Override
	public Article getById(int id) {
		
		return articleDao.findById(id);
	}

}
