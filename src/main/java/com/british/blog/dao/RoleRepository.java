package com.british.blog.dao;

import org.springframework.data.repository.CrudRepository;

import com.british.blog.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByname(String name);
}
