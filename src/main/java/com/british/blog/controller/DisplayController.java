package com.british.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.british.blog.model.Catagory;
import com.british.blog.service.ArticleService;
import com.british.blog.service.CatagoryService;

@Controller
public class DisplayController {
	@Autowired
	CatagoryService catagoryService;
	@Autowired
	ArticleService articleService;
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String get(ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.getCatagory());
		model.addAttribute("articlelist", articleService.getArticles());
		return "user/index";
	}
	@RequestMapping(value="/post",method=RequestMethod.GET)
	public String getPost(@RequestParam("cid") int id,ModelMap model) {
		Catagory catagory= new Catagory();
		catagory.setId(id);
		model.addAttribute("catagorylist", catagoryService.getCatagory());
		model.addAttribute("articlelist", articleService.getArticleByCatagory(catagory));
		return "user/index";
	}
	@RequestMapping(value="/articleDetail",method=RequestMethod.GET)
	public String getArticle(@RequestParam("cid") int id,ModelMap model) {
		model.addAttribute("catagorylist", catagoryService.getCatagory());
		model.addAttribute("articlelist", articleService.getById(id));
		return "user/articledetail";
	}
}
