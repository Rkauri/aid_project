package com.british.blog.service;

import java.util.List;

import com.british.blog.model.Article;
import com.british.blog.model.Catagory;

public interface ArticleService {
public Article addArticle(Article article);
public Article updateArticle(Article article);
public Article getById(int id);
public List<Article> getArticles();
public List<Article> getArticleByCatagory(Catagory catagory);
}
