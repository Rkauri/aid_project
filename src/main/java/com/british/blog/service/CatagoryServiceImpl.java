package com.british.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.british.blog.dao.CatagoryDao;
import com.british.blog.model.Catagory;
@Service
public class CatagoryServiceImpl implements CatagoryService{
@Autowired
CatagoryDao catagoryDao;
	@Override
	public Catagory addCataogry(Catagory catagory) {
		return catagoryDao.save(catagory);
		
	}

	@Override
	public Catagory updateCatagory(Catagory cataogory) {
		return catagoryDao.save(cataogory);		
	}

	@Override
	public List<Catagory> getCatagory() {
		
		return catagoryDao.findAll();
	}

	@Override
	public Catagory getById(int id) {
		
		return catagoryDao.findById(id);
	}

	

}
