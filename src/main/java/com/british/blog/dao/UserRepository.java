package com.british.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.blog.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
	
	User findByUsername(String username);
	
	User findByEmail(String email);
	
}
