package com.british.blog.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.british.blog.model.Catagory;
import com.british.blog.model.User;
import com.british.blog.service.CatagoryService;

@Controller
public class CatagoryController {
@Autowired
CatagoryService catagoryService;

@RequestMapping(value="/catagory",method=RequestMethod.GET)
public String getForm() {
	return "catagory/addCatagory";
}
@RequestMapping(value="/addCatagory",method=RequestMethod.POST)
public String getForm(@ModelAttribute Catagory catagory,RedirectAttributes model) {
	catagory.setCreatedDt(new Date());
	Catagory dbmodel=catagoryService.addCataogry(catagory);
	if (dbmodel != null) {
		model.addFlashAttribute("success", "Action Processed");
	} else {
		model.addFlashAttribute("failure", "Action Process Failed");
	}
	return "redirect:catagory/addCatagory";
}
@RequestMapping(value="/catagorylist",method=RequestMethod.GET)
public String getUsers(ModelMap model) {
	model.addAttribute("catagorylist", catagoryService.getCatagory());
	return "catagory/catagorylist";
}
@RequestMapping(value="/getCatagory",method=RequestMethod.GET)
public String getEditForm(@RequestParam("id") int id,ModelMap model) {
	model.addAttribute("catagorylist",catagoryService.getById(id));
	return "catagory/editCatagory";
}
@RequestMapping(value="/updateCatagory",method=RequestMethod.POST)
public String update(@ModelAttribute Catagory catagory,RedirectAttributes model) {
	catagory.setUpdatedDt(new Date());
	Catagory dbmodel=catagoryService.updateCatagory(catagory);
	if (dbmodel != null) {
		model.addFlashAttribute("success", "Action Processed");
	} else {
		model.addFlashAttribute("failure", "Action Process Failed");
	}
	return "redirect:catagory/editCatagory";
}

}
