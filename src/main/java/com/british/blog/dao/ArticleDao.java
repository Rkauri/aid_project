package com.british.blog.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.british.blog.model.Article;
import com.british.blog.model.Catagory;

public interface ArticleDao extends JpaRepository<Article, Integer> {
List<Article> findByCatagory(Catagory catagory);
Article findById(int id);
}
