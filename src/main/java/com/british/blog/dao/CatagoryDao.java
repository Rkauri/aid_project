package com.british.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.british.blog.model.Catagory;
@Repository
public interface CatagoryDao extends JpaRepository<Catagory, Integer> {
Catagory findById(int id);
}
