package com.british.blog.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tbl_catagory")
public class Catagory {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
private int id;
private String catagoryName;
private Date createdDt;
private Date updatedDt;
//@OneToMany(mappedBy="catagory")
//private List<Article> articlelist;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

public String getCatagoryName() {
	return catagoryName;
}
public void setCatagoryName(String catagoryName) {
	this.catagoryName = catagoryName;
}
public Date getCreatedDt() {
	return createdDt;
}
public void setCreatedDt(Date createdDt) {
	this.createdDt = createdDt;
}
public Date getUpdatedDt() {
	return updatedDt;
}
public void setUpdatedDt(Date updatedDt) {
	this.updatedDt = updatedDt;
}
//public List<Article> getArticlelist() {
//	return articlelist;
//}
//public void setArticlelist(List<Article> articlelist) {
//	this.articlelist = articlelist;
//}

}
