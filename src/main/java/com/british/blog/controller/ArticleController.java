package com.british.blog.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.activation.MimetypesFileTypeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.british.blog.model.Article;
import com.british.blog.service.ArticleService;
import com.british.blog.service.CatagoryService;
@Controller
public class ArticleController {
	@Autowired
	ArticleService articleService;
	@Autowired
	CatagoryService catagoryService;
	
	@RequestMapping(value="/articles",method=RequestMethod.GET)
	public String getForm(ModelMap model) {
		model.addAttribute("catagorylist",catagoryService.getCatagory() );
		return "article/articleform";
	}
	@RequestMapping(value="/addArticle",method=RequestMethod.POST)
	public String getForm(@ModelAttribute Article article,@RequestParam("file") MultipartFile file, RedirectAttributes model) {
		String filePath = "upload/" + file.getOriginalFilename();
		File f = new File(filePath);
		String mimetype = new MimetypesFileTypeMap().getContentType(f);
		if (mimetype.startsWith("image/")) {
			System.out.println("It's an image");
			try {

				byte[] bytes = file.getBytes();
				OutputStream os = new FileOutputStream("src/main/resources/static/" + filePath);
				for (int x = 0; x < bytes.length; x++) {
					os.write(bytes[x]); // writes the bytes
				}
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			model.addFlashAttribute("failure", "Selected file is not image.");
			return "redirect:product";
		}
		article.setImgPath(filePath);
		article.setCreatedDt(new Date());
		Article dbmodel=articleService.addArticle(article);
		if (dbmodel != null) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			model.addFlashAttribute("failure", "Action Process Failed");
		}
		
		return "redirect:articles";
	}
	@RequestMapping(value="/articlelist",method=RequestMethod.GET)
	public String getArticle(ModelMap model) {
		model.addAttribute("articlelist", articleService.getArticles());
		return "article/articlelist";
	}
	@RequestMapping(value="/getArticle",method=RequestMethod.GET)
	public String getEditForm(@RequestParam("id") int id,ModelMap model) {
		model.addAttribute("articlelist", articleService.getById(id));
		model.addAttribute("catagorylist", catagoryService.getCatagory());
		return "article/editarticle";
	}
	@RequestMapping(value="/updateArticle",method=RequestMethod.POST)
	public String update(@ModelAttribute Article article,@RequestParam("file") MultipartFile file,RedirectAttributes model) {

		Article pmodel = new Article();
		pmodel.setImgPath(article.getImgPath());
		String filePath = "upload/" + file.getOriginalFilename();
		if (file.getOriginalFilename() != null && !file.getOriginalFilename().equals("")) {
			File f = new File(filePath);
			String mimetype = new MimetypesFileTypeMap().getContentType(f);
			if (mimetype.startsWith("image/")) {
				System.out.println("It's an image");
				try {

					byte[] bytes = file.getBytes();
					OutputStream os = new FileOutputStream("src/main/resources/static/" + filePath);
					for (int x = 0; x < bytes.length; x++) {
						os.write(bytes[x]); // writes the bytes
					}
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				model.addFlashAttribute("failure", "Selected file is not image.");
				return "redirect:article/articleform";
			}

			pmodel.setImgPath(filePath);
		}

		pmodel.setId(article.getId());
		pmodel.setHeading(article.getHeading());
		pmodel.setHeadingDetail(article.getHeadingDetail());
		pmodel.setDetail(article.getDetail());
		pmodel.setCatagory(article.getCatagory());
		pmodel.setUpdatedDt(new Date());
		Article dbmodel=articleService.updateArticle(article);
		if (dbmodel != null) {
			model.addFlashAttribute("success", "Action Processed");
		} else {
			model.addFlashAttribute("failure", "Action Process Failed");
		}
		
		return "redirect:articlelist";
		
	}
	
}
