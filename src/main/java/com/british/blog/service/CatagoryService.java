package com.british.blog.service;

import java.util.List;

import com.british.blog.model.Catagory;

public interface CatagoryService {
public Catagory addCataogry(Catagory catagory);
public Catagory updateCatagory(Catagory cataogory);
public Catagory getById(int id);
public List<Catagory> getCatagory();
}
